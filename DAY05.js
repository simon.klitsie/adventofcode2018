var fs = require('fs');

var DataSet = [];

var fileinput = fs.readFileSync('data_DAY05.txt').toString();


function FindSolution(input)
{
    //console.log("input " + input + "END");


    var lowercase = "abcdefghijklmnopqrstuvwxyz";
    var uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    var lowercaseArray = lowercase.split('');
    var uppercaseArray = uppercase.split('');


    //console.log("test " + lowercaseArray);

    var solved = false;

    while(solved === false)
    {
        var FoundMatch = false;

        for(l in lowercaseArray)
        {
            var LC = lowercaseArray[l];
            var UC = uppercaseArray[l];

            var str = "([.]*)(["+LC+"]["+UC+"]|["+UC+"]["+LC+"])([.]*)";
            var RegEX = new RegExp(str, "g");

            if(RegEX.test(input))
            {
                FoundMatch = true;
            }        
        }

        if(FoundMatch === false)
        {
            solved = true;
        }
        else
        {
            for(l in lowercaseArray)
            {
                var LC = lowercaseArray[l];
                var UC = uppercaseArray[l];
        
                var str = "([.]*)(["+LC+"]["+UC+"]|["+UC+"]["+LC+"])([.]*)";
                var RegEX = new RegExp(str, "g");
        
                input = input.replace(RegEX, '$1$3');
            }
        }
    }

    return input;

}


var PartOneResult = FindSolution(fileinput)
 
console.log("ANSWER PART ONE: " + PartOneResult.length);

// PART TWO

fileinput = fs.readFileSync('data_DAY05.txt').toString();

var shortest = fileinput.length;

var lowercase = "abcdefghijklmnopqrstuvwxyz";
var uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

var lowercaseArray = lowercase.split('');
var uppercaseArray = uppercase.split('');

for(l in lowercaseArray)
{
    fileinput = fs.readFileSync('data_DAY05.txt').toString();

    var LC = lowercaseArray[l];
    var UC = uppercaseArray[l];

    var str = "([.]*)(["+LC+"]|["+UC+"])([.]*)";
    var RegEX = new RegExp(str, "g");

    fileinput = fileinput.replace(RegEX, '$1$3');
    
    var PartTwoResult = FindSolution(fileinput);

    if(PartTwoResult.length < shortest)
    {
        shortest = PartTwoResult.length;
    }
}

console.log("ANSWER PART TWO: " + shortest);


// ================================================================================================================

// below is the figuring out testing...

var re = /^([^b,B]|^)([b][B]|[B][b])([^b,B]|$)$/;

//console.log(re.test('abBc'));

//console.log(re.test('aBbc'));

//console.log('abBc'.replace(re, '$1$3!!!'));

//===============================================

var re2 = /([^b,B])([b][B]|[B][b])([^b,B])/g;

//console.log('abBcccBbuuuu'.replace(re2, '$1$3'));

//===============================================

var str = "([^b,B])([b][B]|[B][b])([^b,B])";

var re3 = new RegExp(str, "g");

//console.log('abBcccBbuuuu'.replace(re3, '$1$3'));



