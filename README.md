# AdventOfCode2018

Node Package Manager:

https://docs.npmjs.com/getting-started/using-a-package.json 

https://docs.npmjs.com/getting-started/installing-npm-packages-locally 

https://docs.npmjs.com/files/folders.html

https://www.tutorialspoint.com/nodejs/nodejs_restful_api.htm 

npm install jquery --save // use --save to install and add as a dependancy in the package.json file

npm install bootstrap --save // use --save to install and add as a dependancy in the package.json file


Regular Expressions:

https://flaviocopes.com/javascript-regular-expressions/#how-does-it-work


To run this (refresher):

execute in VS Code terminal - 

 npm install
 node DAY05.js
