var fs = require('fs');

var DataSet = [];

var array = fs.readFileSync('data_DAY03.txt').toString().split("\r\n");

// populate DataSet

for(i in array) 
{
    // #1 @ 35,93: 11x13

    var s6 = array[i].substr(array[i].indexOf("#") + 1, array[i].indexOf("@") - array[i].indexOf("#") - 1);

    var st = array[i].substr(array[i].indexOf("@") + 2);
    
    var s1 = st.substr(0,st.indexOf(","));
    var s2 = st.substr(st.indexOf(",") + 1, st.indexOf(":") - st.indexOf(",") - 1);

    var s3 = st.substr(st.indexOf(" ") + 1, st.indexOf("x") - st.indexOf(" ") - 1);
    var s4 = st.substr(st.indexOf("x") + 1, st.length - st.indexOf("x") - 1);

    DataSet.push({
        ID: parseInt(s6),
        FromLeft: parseInt(s1),
        FromTop: parseInt(s2),
        Width: parseInt(s3),
        Height: parseInt(s4)
    });
}

//console.log(DataSet);

var maxWidth = 0;
var maxHeight = 0;

for(item in DataSet)
{
    var t = DataSet[item];

    if(t.FromLeft + t.Width > maxWidth)
    {
        maxWidth = t.FromLeft + t.Width;
    }

    if(t.FromTop + t.Height > maxHeight)
    {
        maxHeight = t.FromTop + t.Height;
    }    
}

//console.log("maxWidth " + maxWidth);
//console.log("maxHeight " + maxHeight);

var TwoDArray = Array(maxWidth).fill().map(() => Array(maxHeight).fill(0));

for(item in DataSet)
{
    var t = DataSet[item];
    
    for(var x = 0; x < t.Width; x++)
    {
        for(var y = 0; y < t.Height; y++)
        {
            TwoDArray[t.FromLeft + x][t.FromTop + y] = TwoDArray[t.FromLeft + x][t.FromTop + y] + 1;           
        }
    }
    //console.log(t);
}


var counter = 0;

for(var x = 0; x < maxWidth; x++)
{
    for(var y = 0; y < maxHeight; y++)
    {
        if(TwoDArray[x][y] >= 2)
        {
            counter = counter + 1;
        } 
    }
}

console.log("counter " + counter);


// part 2

for(item in DataSet)
{
    var found = true;

    var t = DataSet[item];
    
    for(var x = 0; x < t.Width; x++)
    {
        for(var y = 0; y < t.Height; y++)
        {
            if(TwoDArray[t.FromLeft + x][t.FromTop + y] != 1)
            {
                found = false;
            }            
        }
    }

    if(found)
    {
        console.log("ID " + t.ID);
    }

    //console.log(t);
}

