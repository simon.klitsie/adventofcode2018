var fs = require('fs');

var DataSet = [];

var array = fs.readFileSync('data_DAY02.txt').toString().split("\n");

for(i in array) 
{
    var line = array[i];

    DataSet.push({
                    value: line,
                    TWOCount: 0,
                    THREECount: 0
                });
}

const alphabet = "abcdefghijklmnopqrstuvwxyz";

for(i in DataSet) 
{
    //console.log(DataSet[i].value);

    var st = DataSet[i].value.split('').sort().join('');

    //console.log(st);

    for(j in alphabet.split('')) 
    {
        var character = alphabet[j];
        
        var STRregX2 = "^([^" + character + "]*|^)[" + character + "]{2}([^" + character + "]*|$)$";

        var regX2 = new RegExp(STRregX2);

        if(regX2.test(st) === true)
        {
            DataSet[i].TWOCount = 1;
        }

        var STRregX3 = "^([^" + character + "]*|^)[" + character + "]{3}([^" + character + "]*|$)$";

        var regX3 = new RegExp(STRregX3);

        if(regX3.test(st) === true)
        {
            DataSet[i].THREECount = 1;
        }        
    }
}

// ============================================

//var re = /^([^b]*|^)[b]{2}([^b]*|$)$/;

//console.log(re.test('bb'));

//var re2 = /^([^b]*|^)[b]{3}([^b]*|$)$/;

//console.log(re2.test('bbba'));


// ============================================


//var character = "b";

//var str = "^([^" + character + "]*|^)[" + character + "]{2}([^" + character + "]*|$)$";

//console.log(str);

//var reg = new RegExp(str);

//console.log(reg.test('bbb'));

// ============================================


var No2 = 0;
var No3 = 0;

for(i in DataSet) 
{
    if(DataSet[i].TWOCount > 0)
    {
        No2 = No2 + 1;
    }

    if(DataSet[i].THREECount > 0)
    {
        No3 = No3 + 1;
    }    
}

console.log("No2: " + No2);
console.log("No3: " + No3);

console.log("ANSWER: " + No2 * No3);

